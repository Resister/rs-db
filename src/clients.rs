use std::error::Error;

use async_diesel::*;
use async_trait::async_trait;
use diesel::dsl::exists;
use diesel::{
    prelude::*,
    r2d2::{ConnectionManager, Pool},
    select,
};
use serde::Deserialize;

use crate::entities::{NewPairEntity, PairEntity, PokemonEntity, StarwarsEntity};
use crate::models::{ClientBase, Pair, ReadOnlyClientBase, ReadOnlyResource};
use crate::schema::{pairs::dsl::*, pokemons::dsl::*, starwars::dsl::*};
use crate::utils::GQLClient;

pub struct PairClient {
    pool: Pool<ConnectionManager<SqliteConnection>>,
}

#[async_trait]
impl ClientBase for PairClient {
    fn new() -> Self {
        let manager = ConnectionManager::<SqliteConnection>::new("database.sqlite");
        Self {
            pool: Pool::builder().build(manager).unwrap(),
        }
    }
    async fn create(&mut self, pair: Pair) -> Result<Pair, Box<dyn Error>> {
        let pair_entity = NewPairEntity {
            name: pair.name.to_owned(),
            pokemon_id: pair.pokemon.id.to_owned(),
            starwars_id: pair.starwars_char.id.to_owned(),
        };
        let pokemon_entity = PokemonEntity {
            id: pair.pokemon.id.to_owned(),
            name: pair.pokemon.name.to_owned(),
        };
        let starwars_entity = StarwarsEntity {
            id: pair.starwars_char.id.to_owned(),
            name: pair.starwars_char.name.to_owned(),
        };
        diesel::insert_into(pairs)
            .values(pair_entity)
            .execute_async(&self.pool)
            .await?;

        let pokemon_exists = select(exists(pokemons.find(pokemon_entity.id.to_owned())))
            .get_result_async::<bool>(&self.pool)
            .await?;

        if !pokemon_exists {
            diesel::insert_into(pokemons)
                .values(pokemon_entity)
                .execute_async(&self.pool)
                .await?;
        }
        let starwars_exists = select(exists(starwars.find(starwars_entity.id.to_owned())))
            .get_result_async::<bool>(&self.pool)
            .await?;
        if !starwars_exists {
            diesel::insert_into(starwars)
                .values(starwars_entity)
                .execute_async(&self.pool)
                .await?;
        }
        Ok(pair)
    }
    async fn get(&self, pair_id: i32) -> Result<Pair, Box<dyn Error>> {
        let (pair_entity, pokemon_entity, starwars_entity): (
            PairEntity,
            PokemonEntity,
            StarwarsEntity,
        ) = pairs
            .find(pair_id)
            .inner_join(pokemons)
            .inner_join(starwars)
            .get_result_async(&self.pool)
            .await?;
        let pair = Pair {
            id: Some(pair_id),
            name: pair_entity.name,
            pokemon: ReadOnlyResource {
                id: pokemon_entity.id,
                name: pokemon_entity.name,
            },
            starwars_char: ReadOnlyResource {
                id: starwars_entity.id,
                name: starwars_entity.name,
            },
        };
        Ok(pair)
    }
}

pub struct StarwarsClient {
    url: String,
}

#[derive(Deserialize, Clone, PartialEq, Debug)]
struct StarwarsData {
    name: String,
}

#[async_trait]
impl ReadOnlyClientBase for StarwarsClient {
    fn new() -> Self {
        Self {
            url: "https://swapi.dev/api/people".to_owned(),
        }
    }
    async fn get(&self, resource_id: String) -> Result<ReadOnlyResource, Box<dyn Error>> {
        let res = reqwest::Client::new()
            .get(format!("{}/{}", self.url, resource_id))
            .send()
            .await?;

        // todo: figure out how to pass all the fields, maybe extended generic?
        let json = res.json::<StarwarsData>().await?;
        Ok(ReadOnlyResource {
            id: resource_id,
            name: json.name,
        })
    }
}

#[derive(Deserialize)]
struct PokemonData {
    pokemon: ReadOnlyResource,
}

pub struct PokemonClient {
    pub client: GQLClient,
}

#[async_trait]
impl ReadOnlyClientBase for PokemonClient {
    fn new() -> Self {
        Self {
            client: GQLClient::new("https://graphql-pokemon2.vercel.app"),
        }
    }
    async fn get(&self, resource_id: String) -> Result<ReadOnlyResource, Box<dyn Error>> {
        let query = format!("{{pokemon(id: \"{}\"){{name id}}}}", resource_id);
        let PokemonData { pokemon } = self.client.exec::<PokemonData>(query.as_str()).await?;
        Ok(pokemon)
    }
}
