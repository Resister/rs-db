#[macro_use]
extern crate diesel;
#[macro_use]
extern crate log;

use std::io;
use std::sync::Arc;

use actix_web::{web, App, Error, HttpResponse, HttpServer};
use juniper::http::GraphQLRequest;
use juniper::{EmptyMutation, EmptySubscription, FieldResult, RootNode};

use domain::PairUps;
use models::{ClientBase, Pair, ReadOnlyClientBase};

use crate::clients::{PairClient, PokemonClient, StarwarsClient};
use crate::utils::{load_config, setup_logger};

mod clients;
mod domain;
mod entities;
mod models;
mod schema;
mod utils;

#[cfg(test)]
mod domain_test;

pub struct Context {
    pub pair_ups: PairUps<PairClient, PokemonClient, StarwarsClient>,
}

impl juniper::Context for Context {}

pub struct Query;

#[juniper::graphql_object(Context = Context)]
impl Query {
    async fn team(ctx: &Context) -> FieldResult<Pair> {
        let res = ctx.pair_ups.get(1).await.unwrap();
        Ok(res)
    }
}

//  server setup
type Schema = RootNode<'static, Query, EmptyMutation<Context>, EmptySubscription<Context>>;

async fn graphql(
    st: web::Data<Arc<Schema>>,
    data: web::Json<GraphQLRequest>,
) -> Result<HttpResponse, Error> {
    let pair_ups = PairUps::init(PairUps {
        pair_client: PairClient::new(),
        pokemon_client: PokemonClient::new(),
        starwars_client: StarwarsClient::new(),
    });

    let ctx = Context { pair_ups };
    let res = data.execute(&st, &ctx).await;
    Ok(HttpResponse::Ok()
        .content_type("application/json")
        .header("server", "actix")
        .body(serde_json::to_string(&res).unwrap()))
}

#[actix_web::main]
async fn main() -> io::Result<()> {
    // ---- base tooling -----
    let config = load_config().unwrap();
    setup_logger(&config).unwrap();
    let _guard = sentry::init((
        config.sentry_url,
        sentry::ClientOptions {
            release: sentry::release_name!(),
            ..Default::default()
        },
    ));
    info!("Running on {}:{}", config.host, config.port.to_string());

    // ---- boot server -----
    let schema = Arc::new(Schema::new(
        Query {},
        EmptyMutation::new(),
        EmptySubscription::new(),
    ));
    HttpServer::new(move || {
        App::new()
            .data(schema.clone())
            .service(web::resource("graphql").route(web::post().to(graphql)))
    })
    .bind((config.host, config.port))?
    .run()
    .await
}
