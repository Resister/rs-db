use crate::models::{ClientBase, Pair, ReadOnlyClientBase};
use crate::{PairClient, PokemonClient, StarwarsClient};
use std::marker::Copy;

#[derive(Copy, Clone)]
pub struct PairUps<C, P, S> {
    pub pair_client: C,
    pub pokemon_client: P,
    pub starwars_client: S,
}

impl<C: ClientBase, P: ReadOnlyClientBase, S: ReadOnlyClientBase> PairUps<C, P, S> {
    pub fn init(props: PairUps<C, P, S>) -> Self {
        let pair_client = props.pair_client;
        let pokemon_client = props.pokemon_client;
        let starwars_client = props.starwars_client;
        Self {
            pair_client,
            pokemon_client,
            starwars_client,
        }
    }

    pub async fn create(
        &mut self,
        pokemon_id: String,
        starwars_id: String,
    ) -> Result<Pair, Box<dyn std::error::Error>> {
        let starwars_char = self.starwars_client.get(starwars_id).await?;
        let pokemon = self.pokemon_client.get(pokemon_id).await?;
        let name = format!("{} and {}", starwars_char.name, pokemon.name);
        let pair = Pair {
            id: None,
            name,
            pokemon,
            starwars_char,
        };

        let res = self.pair_client.create(pair).await?;
        Ok(res)
    }
    pub async fn get(&self, pair_id: i32) -> Result<Pair, Box<dyn std::error::Error>> {
        Ok(self.pair_client.get(pair_id).await?)
    }
}

impl juniper::Context for PairUps<PairClient, PokemonClient, StarwarsClient> {}
