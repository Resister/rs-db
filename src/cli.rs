#[macro_use]
extern crate diesel;

use std::error::Error;

use crate::clients::{PairClient, PokemonClient, StarwarsClient};
use crate::domain::PairUps;
use crate::models::{ClientBase, ReadOnlyClientBase};

mod clients;
mod domain;
mod entities;
mod models;
mod schema;
mod utils;

#[async_std::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let mut pair_ups = PairUps::init(PairUps {
        pair_client: PairClient::new(),
        pokemon_client: PokemonClient::new(),
        starwars_client: StarwarsClient::new(),
    });
    pair_ups
        .create("UG9rZW1vbjowMDE=".to_owned(), "1".to_owned())
        .await?;
    let res = pair_ups.get(1).await?;
    println!("{:?}", res);
    Ok(())
}
