use std::clone::Clone;
use std::error::Error;

use async_trait::async_trait;
use juniper::GraphQLObject;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug, GraphQLObject)]
pub struct ReadOnlyResource {
    pub id: String,
    pub name: String,
}

#[derive(Deserialize, Clone, PartialEq, Debug, GraphQLObject)]
pub struct Pair {
    pub id: Option<i32>,
    pub name: String,
    pub pokemon: ReadOnlyResource,
    pub starwars_char: ReadOnlyResource,
}

#[async_trait]
pub trait ReadOnlyClientBase {
    fn new() -> Self;
    async fn get(&self, resource_id: String) -> Result<ReadOnlyResource, Box<dyn Error>>;
}

#[async_trait]
pub trait ClientBase {
    fn new() -> Self;
    async fn create(&mut self, pair: Pair) -> Result<Pair, Box<dyn Error>>;
    async fn get(&self, pair_id: i32) -> Result<Pair, Box<dyn Error>>;
}
