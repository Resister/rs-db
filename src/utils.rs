use std::env;
use std::str::FromStr;

use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use validator::Validate;

#[derive(Debug, Serialize, Deserialize)]
struct GQLResponse<T> {
    data: T,
}

#[derive(Debug, Serialize, Deserialize)]
struct Query {
    query: String,
}

pub struct GQLClient {
    pub url: String,
}

impl GQLClient {
    pub fn new(url: &str) -> Self {
        Self {
            url: url.to_string(),
        }
    }
    pub async fn exec<T: DeserializeOwned>(
        &self,
        query: &str,
    ) -> Result<T, Box<dyn std::error::Error>> {
        let q = Query {
            query: query.to_owned(),
        };

        let res = reqwest::Client::new()
            .post(&self.url)
            .json(&q)
            .send()
            .await?;

        let json = res.json::<GQLResponse<T>>().await?;

        Ok(json.data)
    }
}

#[derive(Validate, Deserialize)]
pub struct Config {
    pub log_level: String,
    #[validate(url)]
    pub sentry_url: String,
    pub host: String,
    #[validate(range(min = 80))]
    pub port: u16,
}

pub fn get_var<T: FromStr>(key: &str, default: Option<T>) -> Result<T, String> {
    let res = env::var(key);
    match res {
        Ok(s) => match s.parse::<T>() {
            Ok(val) => Ok(val),
            Err(_) => Err("Couldn't parse envar".to_owned()),
        },
        Err(_) => match default {
            Some(def) => Ok(def),
            None => Err(format!("{}: Environment Variable required", key.to_owned())),
        },
    }
}

pub fn load_config() -> Result<Config, String> {
    let config = Config {
        log_level: get_var("LOG_LEVEL", Some("DEBUG".to_owned()))?,
        sentry_url: get_var("SENTRY_URL", None)?,
        host: get_var("HOST", Some("127.0.0.1".to_owned()))?,
        port: get_var("PORT", Some(3002))?,
    };
    match config.validate() {
        Ok(_) => (),
        Err(e) => panic!("{}", e),
    };
    Ok(config)
}

pub fn setup_logger(config: &Config) -> Result<(), fern::InitError> {
    let level = config.log_level.to_uppercase();
    fern::Dispatch::new()
        .format(|out, message, record| out.finish(format_args!("{}: {}", record.level(), message)))
        .level(match level.as_str() {
            "ERROR" => log::LevelFilter::Error,
            "WARN" => log::LevelFilter::Warn,
            "INFO" => log::LevelFilter::Info,
            "DEBUG" => log::LevelFilter::Debug,
            _ => panic!("Logger only accepts ERROR, WARN, INFO, and DEBUG levels"),
        })
        .chain(std::io::stdout())
        .apply()?;
    Ok(())
}
