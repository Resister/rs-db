#[cfg(test)]
mod describe_pairs {
    use std::error::Error;

    use async_trait::async_trait;
    use rstest::*;

    use crate::domain::PairUps;
    use crate::models::{ClientBase, Pair, ReadOnlyClientBase, ReadOnlyResource};

    const ID: &str = "1";
    const PAIR_ID: i32 = 1;
    const STARWARS_NAME: &str = "Luke Skywalker";
    const POKEMON_NAME: &str = "Bulbasaur";

    struct MockClient {
        pairs: Vec<Pair>,
    }

    #[async_trait]
    impl ClientBase for MockClient {
        fn new() -> Self {
            Self { pairs: vec![] }
        }

        async fn create(&mut self, pair: Pair) -> Result<Pair, Box<dyn Error>> {
            let updated_pair = Pair {
                id: Some(PAIR_ID),
                ..pair.to_owned()
            };
            self.pairs.push(updated_pair);
            Ok(pair)
        }

        async fn get(&self, pair_id: i32) -> Result<Pair, Box<dyn Error>> {
            let res = self
                .pairs
                .iter()
                .find(|pair| pair.id == Some(pair_id))
                .unwrap()
                .to_owned();
            Ok(res)
        }
    }

    #[derive(Clone)]
    struct MockReadOnlyClient {
        resources: Vec<ReadOnlyResource>,
    }

    trait CreateResource {
        fn create(&mut self, resource: ReadOnlyResource) -> ReadOnlyResource;
    }

    impl CreateResource for MockReadOnlyClient {
        fn create(&mut self, resource: ReadOnlyResource) -> ReadOnlyResource {
            let updated_resource = ReadOnlyResource {
                id: ID.to_owned(),
                ..resource
            };
            self.resources.push(updated_resource.to_owned());
            updated_resource
        }
    }

    #[async_trait]
    impl ReadOnlyClientBase for MockReadOnlyClient {
        fn new() -> Self {
            Self { resources: vec![] }
        }
        async fn get(
            &self,
            resource_id: String,
        ) -> Result<ReadOnlyResource, Box<dyn std::error::Error>> {
            let res = self
                .resources
                .iter()
                .find(|resource| resource.id == resource_id)
                .unwrap()
                .to_owned();
            Ok(res)
        }
    }

    #[fixture]
    fn pair_ups() -> PairUps<MockClient, MockReadOnlyClient, MockReadOnlyClient> {
        let pair_client = MockClient::new();
        let mut pokemon_client = MockReadOnlyClient::new();
        let mut starwars_client = MockReadOnlyClient::new();

        pokemon_client.create(ReadOnlyResource {
            id: ID.to_string(),
            name: POKEMON_NAME.to_string(),
        });
        starwars_client.create(ReadOnlyResource {
            id: ID.to_string(),
            name: STARWARS_NAME.to_string(),
        });
        PairUps::init(PairUps {
            pair_client,
            pokemon_client,
            starwars_client,
        })
    }

    #[fixture]
    fn expected() -> Pair {
        Pair {
            id: None,
            name: format!(
                "{} and {}",
                STARWARS_NAME.to_string(),
                POKEMON_NAME.to_string()
            ),
            pokemon: ReadOnlyResource {
                id: ID.to_string(),
                name: POKEMON_NAME.to_string(),
            },
            starwars_char: ReadOnlyResource {
                id: ID.to_string(),
                name: STARWARS_NAME.to_string(),
            },
        }
    }

    #[rstest]
    async fn it_creates_a_pair(
        mut pair_ups: PairUps<MockClient, MockReadOnlyClient, MockReadOnlyClient>,
        expected: Pair,
    ) {
        let actual = pair_ups
            .create(ID.to_string(), ID.to_string())
            .await
            .unwrap();
        assert_eq!(actual, expected);
    }
}
