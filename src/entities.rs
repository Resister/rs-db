use super::schema::{pairs, pokemons, starwars};

#[derive(Queryable, PartialEq, Debug)]
pub struct PairEntity {
    pub id: Option<i32>,
    pub name: String,
    pub pokemon_id: String,
    pub starwars_id: String,
}

#[derive(Insertable, Queryable, PartialEq, Debug)]
#[table_name = "pokemons"]
pub struct PokemonEntity {
    pub id: String,
    pub name: String,
}

#[derive(Insertable, Queryable, PartialEq, Debug)]
#[table_name = "starwars"]
pub struct StarwarsEntity {
    pub id: String,
    pub name: String,
}

#[derive(Insertable)]
#[table_name = "pairs"]
pub struct NewPairEntity {
    pub name: String,
    pub pokemon_id: String,
    pub starwars_id: String,
}
// #[derive(Insertable)]
// #[table_name = "starwars_entities"]
// pub struct NewStarwarsEntity {
//     pub id: String,
//     pub name: String,
// }
// #[derive(Insertable)]
// #[table_name = "pokemon_entities"]
// pub struct NewPokemonEntity {
//     pub id: String,
//     pub name: String,
// }
