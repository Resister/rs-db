use diesel::*;
table! {
    pairs (id) {
        id -> Nullable<Integer>,
        name -> Text,
        pokemon_id -> Text,
        starwars_id -> Text,
    }
}

table! {
    pokemons (id) {
        id -> Text,
        name -> Text,
    }
}

table! {
    starwars (id) {
        id -> Text,
        name -> Text,
    }
}

joinable!(pairs -> pokemons (pokemon_id));
joinable!(pairs -> starwars (starwars_id));
allow_tables_to_appear_in_same_query!(pairs, pokemons, starwars,);
