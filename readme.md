## Tooling
### Base
- Package Manager: **cargo**
- Type Checking: **cargo**
- Formatting: **rustfmt**
- Linting: **clippy**
- Package Security: **cargo-audit**
- Commit Formatter: **git-cz**
- Quality: **clippy**
- Logging: **fern**
- Error Monitoring: **sentry**
- Data Validation: **validator**
### Domain
- Unit Testing: **rstest**
- ORM: **diesel**
- Migrations: **diesel**
- Http Client: **reqwest**


### Graphql
- Web Framework: **actix**
- Graphql Framework: **juniper**
- Data Loaders(n+1): **** 


### Releasing
- Release Mgmt: **semantic release**
- CICD: **gitlab ci**
  - scheduling
    - e2e
    - releases
- performance testing: ****
- chaos engineering?
- dist infra


### CLI-client
- Coverage: tarpaulin
- Framework: **clap**
- Terminal Color: **termcolor-json**
- Compiler: **cargo**
- Http Mock: **mockito**
- Json to Native Gen: **schemafy**
- GraphQL to Native Gen: **juniper-from-schema**
